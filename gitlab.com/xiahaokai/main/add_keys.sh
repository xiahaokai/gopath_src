#!/usr/bin/env bash
COUNT=$1
rm $(pwd)/cmds.txt
for (( i = 1; i <= $COUNT; i++ )); do
    echo "SET KEY_$i $(($i - 1))" >> cmds.txt
done

# shellcheck disable=SC2046
cat $(pwd)/cmds.txt | redis-cli >> /dev/null

