#!/bin/bash

# echo输出斐波那契数列第n项的值
function fib() {
    local n=$1

    if test $((n)) -lt 1; then
        echo -1
        return 2
    fi

    if test $((n)) -lt 3; then
        echo 1
        return 1
    fi

    local a=$(fib $(("$n" - 1)))
    local b=$(fib $(("$n" - 2)))
    # shellcheck disable=SC2046
    echo $(("$a" + "$b"))
    return 0
}

fib 4
