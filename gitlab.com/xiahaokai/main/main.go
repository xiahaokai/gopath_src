package main

import (
	"fmt"
	"runtime"
	"time"
)


func f() {
	i := 0
	for {
		i++
	}
}

func main() {
	runtime.GOMAXPROCS(1)
	go f()
	begin := time.Now().Unix()
	fmt.Printf("begin:%v\n", begin)
	time.Sleep(time.Second * 1)
	end := time.Now().Unix()
	fmt.Printf("end - begin:%v\n", end - begin)
}
